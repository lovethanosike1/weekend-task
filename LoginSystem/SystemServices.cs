﻿using LoginSystem.Events;
using LoginSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoginSystem
{
    public class SystemServices
    {
        public event Notify LoginCompleted;

        private User user;

        User Customer;

        List<User> RegisteredUsers;

        public SystemServices()
        {
            RegisteredUsers = new List<User>() {
                new User() { FirstName = "James", Pin = "3452", AccountNumber = 1453664773, BVN = "13546147354", IsBanned = true },
                new User() { FirstName = "Jane", Pin = "3352", AccountNumber = 0456864773, BVN = "13540974354", IsBanned = true },
                new User() { FirstName = "Dee", Pin = "3852", AccountNumber = 0453664723, BVN = "13567473564", IsBanned = false },
                new User() { FirstName = "Bill", Pin = "3412", AccountNumber = 1453648773, BVN = "13226473584", IsBanned = true }
            };
        }

        public void Register(User model)
        {
            LoginCompleted = CBN.RegisterSuccess;

            user = new User
            {
                FirstName = model.FirstName,
                Pin = model.Pin,
                AccountNumber = model.AccountNumber,
                CreatedTime = DateTime.Now,
                BVN = model.BVN,
                IsBanned = false


            };
            RegisteredUsers.Add(user);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Your Account {model.FirstName} with account number {model.AccountNumber} was created at {user.CreatedTime}\n" +
                $"BVN is {model.BVN}");
            Console.ForegroundColor = ConsoleColor.White;

            LoginCompleted?.Invoke();
            Authentication();

        }
        public void Login()
        {
            LoginCompleted = CBN.Alarm;
            int trial = 0;
            while (trial < 3)
            {
                Console.WriteLine("Enter Firstname:");
                var LoginName = Console.ReadLine();
                Console.WriteLine("Enter Pin");
                var LoginPin = Console.ReadLine();

                Customer = RegisteredUsers.FirstOrDefault((s => s.FirstName == LoginName && s.Pin == LoginPin));
                if (Customer != null)
                {
                    if (Customer.IsBanned == true)
                    {
                        OnLogin();
                        break;

                    }
                    else
                    {
                        OnLogin();
                        LoginOptions();
                        break;
                    }

                }
                else
                {
                    Console.WriteLine("Invalid Details ");
                }

            }

        }
        protected virtual void OnLogin()
        {
            if (!Customer.IsBanned)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Log in Successful");
                Console.ForegroundColor = ConsoleColor.White;
            }
            else
            {
                Console.WriteLine("Sending Email to CBN.....");
                LoginCompleted?.Invoke();
                
            }

        }
        public void LoginOptions()
        {
            start:
            Console.Write("What do you want to do?\nSelcet Options:\n1=Check BVN\n0=Exit");
            try 
            {
                var input = int.Parse(Console.ReadLine());
                switch (input)
                {
                    case 1:
                        CheckBVN(input);
                        break;
                    case 0:
                        Environment.Exit(0);
                        break;
                }
            } 
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                goto start;
            }
            
        }

        public void Authentication()
        {
            Console.WriteLine($"Press 1 to login\nPress 2 to Register new User\nPress 0 to Exit");
            var input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    Login();
                    break;
                case "2":
                    var newApplication = new Application();
                    newApplication.Run();
                    break;
                case "0":
                    Console.WriteLine("Session ended");
                    Environment.Exit(0);
                    break;
                default:
                    input = Application.PromptUser("input");
                    break;

            }

        }
        public void CheckBVN(int accountnumber)
        {
            start:

            try
            {
                Console.WriteLine("Enter Accountnumber(10-digits)");
                accountnumber = int.Parse(Console.ReadLine());

                var customer = RegisteredUsers.FirstOrDefault(c => c.AccountNumber == accountnumber);
                if (customer != null)
                {
                    Console.WriteLine("Your BVN is: " + customer.BVN);
                }
                else
                {
                    Console.WriteLine("NO BVN found");
                    Console.WriteLine("Get Registered");
                    Register(Customer);
                }
            }
            catch(Exception ex) 
            {
                Console.WriteLine(ex.Message);
                goto start;
            }
            

      
            
            


        }

    }
}

