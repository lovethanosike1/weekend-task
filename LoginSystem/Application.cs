﻿
using LoginSystem.Events;
using LoginSystem.Interfaces;
using LoginSystem.Models;
using System;
using System.Text;

namespace LoginSystem
{
    public class Application : IApplication
    {

        private static int accountnumber = 1034991010;

        string _BVN = "0900765431";
        public static User Customer = new User();

        public Application()
        {

            accountnumber++;
        }
        public void Run()
        {
            var menu = new StringBuilder();
            menu.Append("Hello User!");
            menu.AppendLine("You've been redirected to Register");


            Console.WriteLine(menu.ToString());
            Console.WriteLine("Enter your Firstname:");
            var firstName = Console.ReadLine().Trim();
            while (string.IsNullOrWhiteSpace(firstName) || firstName.Split().Length > 1)
            {
                firstName = PromptUser("firstname");
            }


            Console.WriteLine("Enter your Lastname");
            var lastName = Console.ReadLine().Trim();
            while (string.IsNullOrWhiteSpace(lastName) || lastName.Split().Length > 1)
            { lastName = PromptUser("lastname"); }


            Console.WriteLine("Do you have a BVN?");
            Console.WriteLine("Press 1 = Yes; 0 = No");
            var input = Console.ReadLine();
            if (BVNStatus(input) == true)
            {
                while (true)
                {
                    Console.WriteLine("Enter BVN:\nBVN should be 10-digits");
                    var bvn = Console.ReadLine();
                    if (!IsNumeric(bvn) || bvn.Length != 10 ||string.IsNullOrWhiteSpace(bvn))
                    {
                        Console.WriteLine("Invalid Input. Enter a valid BVN");
                    }
                    else
                    {
                        Console.WriteLine("BVN is valid");
                        bvn = _BVN;
                        break;

                    }
                }

            }
            else
            {
                Console.WriteLine("Enroll for BVN");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("You've been enrolled.");
                Console.ForegroundColor = ConsoleColor.White;

            }


            Customer = new User
            {
                FirstName = firstName,
                Pin = Application.GetPin(),
                AccountNumber = Application.accountnumber,
                BVN = _BVN

            };
            SystemServices systemServices = new SystemServices();
            systemServices.Register(Customer);

           

        }
        public static string PromptUser(string fieldName)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Please enter a valid {fieldName}");
            Console.ForegroundColor = ConsoleColor.White;

            return Console.ReadLine().Trim();
        }
        public static string GetPin()
        {
        start:
            Console.WriteLine("Select Pin: \nInstruction:Pin should be 4-Digits");

            var pin = Console.ReadLine();
            if (pin.Trim().Length == 4)
            {
                if (!IsNumeric(pin))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Pin is not numeric");
                    Console.ForegroundColor = ConsoleColor.White;

                    goto start;
                }
                else
                {
                    Console.WriteLine("Confirm your pin");
                    var confirmPin = Console.ReadLine();

                    var res = PinValidator(pin, confirmPin);
                    if (res == pin)
                    {
                        return pin;
                    }
                    else
                    {
                        Console.WriteLine(res);
                        goto start;

                    }
                }

            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Pin should be 4-digits");
                Console.ForegroundColor = ConsoleColor.White;

                goto start;
            }


        }
        private static string PinValidator(string pin, string confirmPin)
        {
            if (pin.Trim().Length == 4)
            {
                if (pin == confirmPin)
                {
                    return pin;
                }
                else
                {
                    return "pin did not match";
                }

            }
            else
            {
                return "Invalid pin.Pin should be 4-digits";
            }

        }
        public static bool IsNumeric(string pin)
        {
            double test;
            return double.TryParse(pin, out test);

        }

        public static bool BVNStatus(string input)
        {
            bool HasBVN = false;

            switch (input)
            {
                case "1":
                    HasBVN = true;
                    return HasBVN;
                case "0":
                    HasBVN = false;
                    return HasBVN;
                default:
                    input = PromptUser("input");
                    break;
            }
            return HasBVN;


        }


    }
}
