﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginSystem.Models
{
    public class User
    {
        public bool IsBanned { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Pin { get; set; }
        public int AccountNumber { get; set; }
        public string BVN { get; set; }
        public DateTime CreatedTime { get; set; }
    }
}
