﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManipulatingLIst
{
    public class StandardOperators
    {
        public static List <string> Words = new List<string>();
        

        public static void GetWords()
        {
            Console.WriteLine("This is a query system that shuffles sorted words\n");
            Console.Write("Enter words or a sentence:");
            string input = Console.ReadLine();
     
            var words = input.Split();
            foreach(var w in words)            {
                Words.Add(w);
            }
            Console.WriteLine("Unsorted\n------------------------------");
            foreach(var word in Words)
            {
                Console.WriteLine(word);
            }

            
 
        }
        public static IOrderedEnumerable<string> Sort(List<string> list)
        {
            //using Querysyntax
            var sortQuerySyntax = from word in list
                                  orderby word ascending
                                  select word;


            //to show execution
            Console.WriteLine("\n\nSorted\n--------------------------");
            foreach (var w in sortQuerySyntax)
            {
                Console.WriteLine(w);
            }

            // using MethodSyntax
            var sortMethodSyntax = list.OrderBy(w => w);
            //to show excution
            Console.WriteLine("\n\nSorted\n--------------------------");

            foreach (var w in sortMethodSyntax)
            {
                Console.WriteLine(w);
            }

            return sortMethodSyntax;
   
        }
        public static void Shuffle(IOrderedEnumerable<string> list)
        {
             
            Random random = new Random();//used to shuffle
            //using Querysyntax
            var shuffleQuerySyntax = from word in list
                                     orderby random.Next()
                                     select word;
            Console.WriteLine("\n\nShuffle\n-----------------------------");
            foreach(var word in shuffleQuerySyntax)
            {
                Console.WriteLine(word);
            }

            var shuffleMethodSyntax = list.OrderBy(w=>random.Next());

            Console.WriteLine("\n\nShuffle\n-----------------------------");
            foreach (var word in shuffleMethodSyntax)
            {
                Console.WriteLine(word);
            }

        }
    }
}
