﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CharMapping
{
    class Program
    {
        static void Main(string[] args)
        {

            CharMapping();
            
        }
        static void CharMapping()
        {
            start: 
            string input;
            Console.WriteLine("This is a character digit mapper system\nOnly the Special characters of eg. )(*&^%$#@! are valid");
            Console.WriteLine("Make an Input: ");
            input = Console.ReadLine();
            bool IsValid = true;


            foreach (var c in input)
            {
                
                Regex reg = new Regex("[)(*&^%$#@!]");
                Match matched = reg.Match(c.ToString());
                if (matched.Success)
                {
                    continue;
                }
                else
                {

                    IsValid = !IsValid;
                    break;
                }
                
            }
            switch (IsValid)
            {
                case true:
                    Console.WriteLine("Input is valid");
                    MapChar(input);
                    break;
                case false:
                    Console.WriteLine("input is Invalid");
                    goto start;
            }
            

        }
        static void MapChar(string input)
        {
            Dictionary<int, char> digitCharMap = new Dictionary<int, char>()
            {
                {1,')' },
                {2,'(' },
                {3,'*' },
                {4,'&' },
                {5,'^' },
                {6,'%' },
                {7,'$' },
                {8,'#' },
                {9,'@' },
                {0,'!' }
               
            };

            StringBuilder Result = new StringBuilder();
            foreach (var c in input)
            {
                //Querysyntax
                var CharQuerySyntax = from kvp in digitCharMap
                                      where kvp.Value == c
                                      select kvp.Key;
                foreach(var coll in CharQuerySyntax)
                {
                    Result.Append(coll);
                }
            }
            Console.WriteLine(Result.ToString());


        }

    }
}
